# Usabilidad. Diseño plano
Realiza una tabla resumen de las acciones de diseño que previenen los problemas de usabilidad del diseño plano.

| Problema de usabilidad | Recomendación |
| -- | -- |
| Comunicar qué elementos son seleccionables o se les puede hacer click | Utiliza un diseño híbrido entre el diseño plano y esqueuomórfico que permita al usuario reconocer estos elementos (añadir profundidad, sombras, ícono...). |
| Baja densidad de información | No abusar del diseño minimalista mostrando poca información, puesto que podría sacrificar el objetivo principal del sitio web |
| Contraste inapropiada entre texto e imágenes | En el caso de usar texto encima de una imagen asegurarse de usar un contraste de color adecuado para diferenciar el texto de la imagen |

## Artículos consultados
* https://www.nngroup.com/articles/flat-design-best-practices/
* https://es.hideout-lastation.com/flat-2-0-how-it-solves-flat-design-s-usability-problems
